module Control.Monad.Freer.Catching where

import           Control.Exception
import           Control.Monad
import           Control.Monad.Freer
import           Control.Monad.Freer.Internal
import           Data.OpenUnion.Internal

data Catching f e a where
    Catching :: forall f e a . (Exception e)
             => (forall r . Member f r => Eff r a)
             -> Catching f e (Either e a)

catching :: forall e f a r
          . (Exception e, Member (Catching f e) r)
         => Eff '[f] a
         -> Eff r (Either e a)
catching f = send @(Catching f e) $ Catching (generalize f)

generalize :: Member f r => Eff '[f] a -> Eff r a
generalize (Val x)  = return x
generalize (E u' q) = send (extract u') >>= qComp q generalize

runCatching :: forall e r v f
             . (Member IO r, Member IO (f ': r))
            => (forall a q . Member IO q => Eff (f ': q) a -> Eff q a)
            -> Eff (Catching f e : f : r) v
            -> Eff r v
runCatching runner = runner . runNat handler
    where handler :: Catching f e a -> IO a
          handler (Catching f) = do
            x <- try . runM $ evaluate <$> runner f
            case x of
              Left e -> return $ Left e
              Right y -> try y


runCatching2 :: forall e r v f g
             . (Member IO r, Member IO (f : r), Member IO (f : g : r))
            => (forall a q. Member g q => Eff (f : q) a -> Eff q a)
            -> (forall a q. Member IO q => Eff (g : q) a -> Eff q a)
            -> Eff (Catching f e : f : g : r) v
            -> Eff r v
runCatching2 runner1 runner2 = runner2 . runner1 . runNat handler
    where handler :: Catching f e a -> IO a
          handler (Catching f) = do
            x <- try . runM $ evaluate <$> (runner2 . runner1 $ f)
            case x of
              Left e -> return $ Left e
              Right y -> try y
